from django.shortcuts import render
from todos.models import TodoList


# Create your views here.
def todo_list_list(request):
    todolistlist = TodoList.objects.all()
    context = {"todo_list": todolistlist}
    return render(request, "todos/home.html", context)
